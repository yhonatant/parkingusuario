package com.parkinglot.parkinglot.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.parkinglot.parkinglot.MainActivity;
import com.parkinglot.parkinglot.R;
import com.parkinglot.parkinglot.model.Clases.Usuario;
import com.parkinglot.parkinglot.model.Comandos.ComandoValidarCorreoFirebase;
import com.parkinglot.parkinglot.model.Modelo;
import com.parkinglot.parkinglot.model.utility.Utility;

import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Registrarse extends Activity  implements ComandoValidarCorreoFirebase.OnValidarCorreoFirebaseChangeListener  {

    Usuario usuario;

    EditText txt_nombre, txt_apellido, txt_celular, txt_correo, txt_passwor,txt_passwor1,txt_placa,txt_modelo, txt_tarjetadepropiedad;
    private RelativeLayout mRlView;
    Modelo modelo = Modelo.getInstance();
    String token = "";
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseAuth.AuthStateListener mAuthListener;
    ComandoValidarCorreoFirebase comandoValidarCorreoFirebase;
    private ProgressDialog progressDialog;
    private static final String TAG ="AndroidBash";
    Utility utility;
    SweetAlertDialog pDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registrarse);


        if (savedInstanceState != null) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
            return;
        }



        utility = new Utility();
        usuario =  null;
        boolean result = Utility.checkPermission(Registrarse.this);

        txt_nombre = (EditText)findViewById(R.id.txt_nombre);
        txt_apellido = (EditText)findViewById(R.id.txt_apellido);
        txt_celular = (EditText)findViewById(R.id.txt_celular);
        txt_correo = (EditText)findViewById(R.id.txt_correo);
        txt_passwor = (EditText)findViewById(R.id.txt_passwor);
        txt_passwor1 = (EditText)findViewById(R.id.txt_passwor1);
        txt_placa = (EditText)findViewById(R.id.txt_placa);
        txt_modelo = (EditText)findViewById(R.id.txt_modelo);
        txt_tarjetadepropiedad = (EditText)findViewById(R.id.txt_tarjetadepropiedad);
        mRlView = (RelativeLayout) findViewById(R.id.mRlView);

        token = FirebaseInstanceId.getInstance().getToken();
        comandoValidarCorreoFirebase = new ComandoValidarCorreoFirebase(this);

    }

    public void validar(View v){

        if(txt_nombre.getText().toString().length() < 3){
            txt_nombre.setError("Información muy corta");
        }
        else if(txt_apellido.getText().toString().length() < 3){
            txt_apellido.setError("Información muy corta");
        }

        else if(txt_celular.getText().toString().length() < 10){
            txt_celular.setError("Información muy corta");
        }

        else if(txt_correo.getText().toString().length() < 5){
            txt_correo.setError("Email muy corta");
        }

        else if (!validarEmail(txt_correo.getText().toString())) {
            txt_correo.setError("Email no válido");
        }

        else if(txt_placa.getText().toString().length() < 2){
            txt_placa.setError("Información muy corta");
        }

        else if(txt_modelo.getText().toString().length() < 2){
            txt_modelo.setError("Información muy corta");
        }

        else if(txt_tarjetadepropiedad.getText().toString().length() < 3){
            txt_tarjetadepropiedad.setError("Información muy corta");
        }
        else if(txt_passwor.getText().toString().length() < 8){
            txt_passwor.setError("Contraseña muy corta, minimo 7 caracteres");
        }
        else if(txt_passwor1.getText().toString().length() < 8){
            txt_passwor1.setError("Contraseña muy corta, minimo 7 caracteres");
        }

        else if(!txt_passwor.getText().toString().equals(txt_passwor.getText().toString())){
            txt_passwor.setError("Las contraseña no coinciden.");
        }
        else{


            if (utility.estado(getApplicationContext())) {
                loadswet("Validando la información...");
                comandoValidarCorreoFirebase.checkAccountEmailExistInFirebase(txt_correo.getText().toString());

            }else{
                alerta("Sin Internet","Valide la conexión a internet");
            }
        }
    }


    //posgres dialos sweetalert

    public void loadswet(String text){

        try {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        }catch (Exception e){

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }


    //validar email
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    //alerta swit alert
    //alerta
    public void alerta(String titulo,String decripcion){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                })

                .show();
    }

    //alerta
    public void alertaOK(String titulo,String decripcion){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                        Intent i = new Intent(getApplicationContext(), Login.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

                    }
                })

                .show();
    }


    public void terminos(View v){
        Intent i = new Intent(getApplicationContext(), Teminos.class);
        startActivity(i);
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        finish();
    }



    //metod interface

    @Override
    public void cargoValidarCorreoFirebase() {
        String nombre = txt_nombre.getText().toString();
        String apellido = txt_apellido.getText().toString();
        String celular = txt_celular.getText().toString();
        String correo = txt_correo.getText().toString();
        String password = txt_passwor.getText().toString();
        String placa = txt_placa.getText().toString();
        String modeloVehiculo = txt_modelo.getText().toString();
        String tarjetadepropiedad = txt_tarjetadepropiedad.getText().toString();
        String token = this.token;
        usuario = new Usuario("",nombre,apellido,celular,correo,password,token,modelo.latitud,modelo.longitud,0,0,true,placa,modeloVehiculo,tarjetadepropiedad);

        comandoValidarCorreoFirebase.registroUsuario(usuario);
    }

    @Override
    public void cargoValidarCorreoFirebaseEroor() {
        txt_correo.setError("Email ya registrado");
        hideDialog();

    }

    @Override
    public void setUsuarioListener() {

        hideDialog();
        alertaOK("Registro exitoso", "Inicie sesion");
    }

    @Override
    public void errorSetUsuario() {
        hideDialog();
        alerta("Error Resgisto","Algo salió malcon el registro verifique su conexión.");
    }

    @Override
    public void errorCreacionUsuario() {
        hideDialog();

        alerta("Error Resgisto","Algo salió malcon el registro verifique su conexión");
    }
}