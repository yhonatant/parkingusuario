package com.parkinglot.parkinglot.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.parkinglot.parkinglot.R;
import com.parkinglot.parkinglot.model.Clases.Usuario;
import com.parkinglot.parkinglot.model.Comandos.ComandoPerfilFragment;
import com.parkinglot.parkinglot.model.Modelo;
import com.parkinglot.parkinglot.model.utility.Utility;

import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Perfil extends Activity implements ComandoPerfilFragment.OnPerfilChangeListener{

    Usuario usuario;

    EditText txt_nombre, txt_apellido, txt_celular, txt_correo;
    Modelo modelo = Modelo.getInstance();
    String token = "";
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    ComandoPerfilFragment comandoPerfil;
    private ProgressDialog progressDialog;
    private static final String TAG ="AndroidBash";
    Utility utility;
    Button btncerrar;
    Button update;
    SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_perfil);


        //instanciamos la clase utility
        utility = new Utility();
        usuario =  null;

        txt_nombre = (EditText)findViewById(R.id.txt_nombre);
        txt_apellido = (EditText)findViewById(R.id.txt_apellido);
        txt_celular = (EditText)findViewById(R.id.txt_celular);
        txt_correo = (EditText)findViewById(R.id.txt_correo);
        update = (Button)findViewById(R.id.btnupdate);
        btncerrar = (Button)findViewById(R.id.btncerrar);
        comandoPerfil =  new ComandoPerfilFragment(this);



        if (utility.estado(getApplicationContext())) {
            comandoPerfil.getUsuario();
        }
        else {
            alerta("Sin Internet","Valide la conexión a internet");
        }


        btncerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarSesion();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validar();
            }
        });
    }


    //alerta swit alert
    //alerta
    public void alerta(String titulo,String decripcion){

       try{
           new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.WARNING_TYPE)
                   .setTitleText(titulo)
                   .setContentText(decripcion)
                   .setConfirmText("Aceptar")
                   .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                       @Override
                       public void onClick(SweetAlertDialog sDialog) {

                           sDialog.dismissWithAnimation();

                       }
                   })

                   .show();
       }catch (Exception ex){
           Log.v("ex","ex: "+ex.getMessage());
       }
    }


    @Override
    public void cargoUSuario() {

        hideDialog();
        txt_nombre.setText("" + modelo.usuario.getNombre());
        txt_apellido.setText("" + modelo.usuario.getApellido());
        txt_celular.setText("" + modelo.usuario.getCelular());
        txt_correo.setText("" + modelo.usuario.getCorreo());
    }

    @Override
    public void setUsuarioListener() {
      //  hideDialog();
      //  alerta("Actualización","Datos Actualizados");
        Toast.makeText(getApplicationContext(),"Datos Actualizados", Toast.LENGTH_LONG).show();
    }

    @Override
    public void errorSetUsuario() {
        //hideDialog();
        //alerta("Eror","Error con el regitro");
        Toast.makeText(getApplicationContext(),"Error con el regitro", Toast.LENGTH_LONG).show();
    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    //Cerrar Session

    public void cerrarSesion(){

        if(modelo.tipoLogin.equals("normal")){
            cerrarNormal();
        }
    }

    public void cerrarNormal(){
        Logout();
    }

    public void Logout(){
        try {
            modelo.tipoLogin = "";
            user =  null;
            modelo.uid = "";
            mAuth.signOut();
            mAuth.getInstance().signOut();
            Toast.makeText(getApplicationContext(),  "Cerrarndo sessión", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(getApplicationContext().getApplicationContext(), Login.class);
            startActivity(i);

        }
        catch (Exception e){}
    }



    public void validar(){

        if(txt_nombre.getText().toString().length() < 3){
            txt_nombre.setError("Información muy corta");
        }
        else if(txt_apellido.getText().toString().length() < 3){
            txt_apellido.setError("Información muy corta");
        }

        else if(txt_celular.getText().toString().length() < 10){
            txt_celular.setError("Información muy corta");
        }

        else if(txt_correo.getText().toString().length() < 5){
            txt_correo.setError("Email muy corta");
        }

        else if (!validarEmail(txt_correo.getText().toString())) {
            txt_correo.setError("Email no válido");
        }
        else{

            usuario = new Usuario();
            usuario.setNombre(txt_nombre.getText().toString());
            usuario.setApellido(txt_apellido.getText().toString());
            usuario.setCelular(txt_celular.getText().toString());

            if (utility.estado(getApplicationContext())) {
                loadswet("Validando la información...");
                comandoPerfil.actualizarUsuario(usuario);
            }else{
                alerta("Sin Internet","Valide la conexión a internet");
            }
        }
    }


    //posgres dialos sweetalert

    public void loadswet(String text){

        try {
            pDialog = new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        }catch (Exception e){

        }
    }


    //validar email
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}