package com.parkinglot.parkinglot.view;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.location.Geocoder;
import com.emredavarci.floatingactionmenu.FloatingActionMenu;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parkinglot.parkinglot.R;
import com.parkinglot.parkinglot.Tarjeta;
import com.parkinglot.parkinglot.model.Clases.MapUtils;
import com.parkinglot.parkinglot.model.Clases.Parqueadero;
import com.parkinglot.parkinglot.model.Comandos.ComandoParqueadero;
import com.parkinglot.parkinglot.model.Comandos.ComandoTerminosYCondiciones;
import com.parkinglot.parkinglot.model.Modelo;
import com.parkinglot.parkinglot.model.utility.Utility;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        ComandoParqueadero.OnParqueaderosChangeListener{

    private GoogleMap mMap;
    MarkerOptions markerOptions;
    Modelo modelo = Modelo.getInstance();
    ComandoParqueadero comandoParqueadero;
    Utility utility;
    SweetAlertDialog pDialog;
    private Circle mCircle;
    FloatingActionMenu menu;

    //layout del adapter del market
    LinearLayout layutdetalle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        markerOptions = new MarkerOptions();
        mapFragment.getMapAsync(this::goolemapa);
        mapFragment.getMapAsync(this);

        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), MapsActivity.class);
            startActivity(i);
            finish();
            return;
        }


         menu = (FloatingActionMenu) findViewById(R.id.floatingMenu);
        menu.setClickEvent(new FloatingActionMenu.ClickEvent() {
            @Override
            public void onClick(int index) {
                Log.d("TAG", String.valueOf(index)); // index of clicked menu item
               // Toast.makeText(getApplicationContext(),"index:" + index, Toast.LENGTH_LONG).show();
                if(index==0){
                    Intent i = new Intent(getApplicationContext(), Perfil.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                }
                if(index==1){
                    Intent i = new Intent(getApplicationContext(), Historial.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                }
                if(index==2){
                    Intent i = new Intent(getApplicationContext(), Teminos.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                }
                if(index==3){
                    Intent i = new Intent(getApplicationContext(), Tarjeta.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                }

            }
        });


        utility = new Utility();

        if (utility.estado(getApplicationContext())) {
          //  loadswet("Cargando la información...");
        comandoParqueadero = new ComandoParqueadero(this);
        comandoParqueadero.getListParqueadero();
        mapa();
        }else{
            alerta("Sin Internet","Valide la conexión a internet");
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        modelo.mMap = googleMap;

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //  googleMap.setMyLocationEnabled(true);

        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);



        goolemapa(modelo.mMap );


    }


    private void drawMarkerWithCircle(LatLng position){
        double radiusInMeters = 400.0;
        int strokeColor = 0x000000ff; //red outline
        int shadeColor = 0x440000ff; //opaque red fill

        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        mCircle = modelo.mMap.addCircle(circleOptions);



    }



    public void goolemapa(GoogleMap mMap){

        modelo.mMap = mMap;
        //5.059288, -75.497652
        LatLng ctg = new LatLng(modelo.latitud, modelo.longitud);// colombia
        CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(15).bearing(0).tilt(0).build();
        CameraUpdate cam3 =
                CameraUpdateFactory.newCameraPosition(possiCameraPosition);
        mMap.animateCamera(cam3);
        // mMap.addMarker(new MarkerOptions().position(ctg).title("Mi ubicación"));

        // float verde = BitmapDescriptorFactory.HUE_GREEN;
        //marcadorColor(modelo.latitud, modelo.longitud,"Pais Colombia", verde,mMap);
        marcadorImg(modelo.latitud, modelo.longitud,"Pais",mMap);
        //setLocation();

        try {

            modelo.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(modelo.latitud, modelo.longitud), 15));

            MarkerOptions options = new MarkerOptions();

            // Setting the position of the marker

            options.position(new LatLng(modelo.latitud, modelo.longitud));

            //googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

            LatLng latLng = new LatLng(modelo.latitud, modelo.longitud);
            drawMarkerWithCircle(latLng);


            modelo.mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    float[] distance = new float[2];



                    Location.distanceBetween( location.getLatitude(), location.getLongitude(),
                            mCircle.getCenter().latitude, mCircle.getCenter().longitude, distance);



                }
            });




        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void marcadorColor(double lat, double lng, String  pais, GoogleMap mMap){
        mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(pais).icon(BitmapDescriptorFactory.defaultMarker()));
    }


    protected Marker createMarker(String key,double lat, double lng, String  pais, GoogleMap mMap) {

        MapUtils mapUtils = new MapUtils(getApplicationContext());
        Bitmap bitmap = mapUtils.GetBitmapMarker(getApplicationContext(), R.drawable.parkinlotgps, "1");


        //Bitmap bitmap =  GetBitmapMarker(getApplicationContext(), R.drawable.parkinlotgps, "1");
       // mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(LayoutInflater.from(getApplicationContext()),""+pais));

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .anchor(0.5f, 0.5f)
                .title(pais)
                .snippet(key)
                .icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
               // .icon(BitmapDescriptorFactory.fromResource(R.drawable.parkinlotgps)));
    }

    private void marcadorImg(double lat, double lng, String  pais, GoogleMap mMap){
        //modelo.mMap = mMap;
        //   MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lng)).title("Enfermera");

        LatLng  latLng = new LatLng(lat,lng);

        modelo.mMap.clear();
        modelo.mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title("Parkint lot")
                .snippet(getCity(latLng))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car2))
                .draggable(true)
        );


        if (modelo.mMap != null) {
            modelo.mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {
                    Log.v("1","1");
                }

                @Override
                public void onMarkerDrag(Marker marker) {
                    Log.v("2","2");
                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    Log.v("3","3");
                    getCity(marker.getPosition());

                    modelo.latitud = marker.getPosition().latitude;
                    modelo.longitud = marker.getPosition().latitude;
                    goolemapa(modelo.mMap);
                    listaMapa();
                }
            });


            //onclik parqueo
            /// modelo.mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(LayoutInflater.from(getApplicationContext()),"ok",getApplicationContext()));
            modelo.mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {


                    Toast.makeText(getApplicationContext(),"- *"+marker.getSnippet(),Toast.LENGTH_LONG).show();
                   // alerta("Alerta","adapter marrket");
                    Intent i = new Intent(getApplicationContext(), Parqueo.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                }
            });
            modelo.mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = getLayoutInflater().inflate(R.layout.infowindow_layout,null);
                    TextView nomCompagnie = (TextView) v.findViewById(R.id.info_window_nombre);
                    TextView cupos = (TextView) v.findViewById(R.id.cupos);
                    TextView horario = (TextView) v.findViewById(R.id.horario);
                    Button reserva = (Button) v.findViewById(R.id.reservar);
                     layutdetalle = (LinearLayout) v.findViewById(R.id.layutdetalle);
                    layutdetalle.setClickable(true);


                    if(marker.getTitle().equals("Parkint lot")){
                        layutdetalle.setVisibility(View.GONE);

                    }else{
                        layutdetalle.setVisibility(View.VISIBLE);
                    }
                    Parqueadero parqueadero = new Parqueadero();
                    for(int i=0; i < modelo.listParqueaderos.size(); i++){
                        if(modelo.listParqueaderos.get(i).getKey().equals(marker.getSnippet())){
                            parqueadero = modelo.listParqueaderos.get(i);
                            break;
                        }
                    }


                    nomCompagnie.setText(marker.getTitle());
                    cupos.setText("Disponibilidad: "+parqueadero.getDisponibles());
                    horario.setText("Horario: "+parqueadero.getHorario());

                    layutdetalle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getApplicationContext(),"Hola",Toast.LENGTH_LONG).show();
                        }
                    });

                    return v;
                }
            });
            modelo.mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    //Toast.makeText(getApplicationContext(),"click"+marker.getTitle(), Toast.LENGTH_SHORT).show();

                    return false;
                }
            });


           for(int i=0;i< modelo.listParqueaderos.size();i++)
            {
                modelo.mMap.addMarker(new MarkerOptions().position(new LatLng(modelo.listParqueaderos.get(i).getLat(), modelo.listParqueaderos.get(i).getLon())).title(""+i+"").snippet(modelo.listParqueaderos.get(i).getKey())).showInfoWindow();
                // Log.d("wiwwww", "heereee");
            }




        }

    }


    public  String getCity(LatLng posicion){

        Geocoder geocoder;
        List<Address> addresses;
        String dir  = "";


        try {
            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

            List<Address> list = geocoder.getFromLocation(
                    posicion.latitude, posicion.longitude, 1);
            if (!list.isEmpty()) {
                Address DirCalle = list.get(0);


                dir = DirCalle.getAddressLine(0);

                // Toast.makeText(getApplicationContext(),"Mi direccion es "+ dir, Toast.LENGTH_SHORT).show();
                String[] parts = dir.split(",");
                String direc = parts[0];


            }
        }catch (Exception e){
            String ex = e.getMessage();
            e.printStackTrace();
        }
        return dir;
    }

    //GPS
    public void mapa(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }
    }

    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        MapsActivity.Localizacion Local = new MapsActivity.Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);
        Toast.makeText(getApplicationContext(),"Localización agregada", Toast.LENGTH_SHORT).show();

    }
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }
    public void setLocation(Location loc) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);


                    String dir =  DirCalle.getAddressLine(0);

                    // Toast.makeText(getApplicationContext(),"Mi direccion es "+ dir, Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void getParqueadero() {
        try {
            Log.v("Cantidad","Cantidad: "+modelo.listParqueaderos.size());
            int cantidad = modelo.listParqueaderos.size();


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    int listParqueadero = modelo.listParqueaderos.size();
                    Log.v("client size",  ": " +  listParqueadero);

                    if(listParqueadero > 0){
                        listaMapa();
                    }
                }
            },1000);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
     Log.v("hasCapture", "estado "+hasCapture);
    }

    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        MapsActivity mainActivity;
        public MapsActivity getMainActivity() {
            return mainActivity;
        }
        public void setMainActivity(MapsActivity mainActivity) {
            this.mainActivity = mainActivity;
        }
        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion
            loc.getLatitude();
            loc.getLongitude();


            modelo.latitud = loc.getLatitude();
            modelo.longitud = loc.getLongitude();
            this.mainActivity.setLocation(loc);
        }
        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado

            Toast.makeText(getApplicationContext(),"GPS Desactivado", Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado

            Toast.makeText(getApplicationContext(),"GPS Activado", Toast.LENGTH_SHORT).show();
            goolemapa(mMap);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }
    }

    //fin gps



    //posgres dialos sweetalert

    public void loadswet(String text){

        try {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(text);
            pDialog.setCancelable(false);
            pDialog.show();

        }catch (Exception e){

        }

    }


    //oculatomos el dialog
    private void hideDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }



    //alerta swit alert
    //alerta
    public void alerta(String titulo,String decripcion){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();

                    }
                })

                .show();
    }

    //alerta
    public void alertaOK(String titulo,String decripcion){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(titulo)
                .setContentText(decripcion)
                .setConfirmText("Aceptar")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                        Intent i = new Intent(getApplicationContext(), Login.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

                    }
                })

                .show();
    }





    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_home) {
            Toast.makeText(this, "Se presionó el ícono del menu", Toast.LENGTH_LONG).show();
            return true;
        }

           return super.onOptionsItemSelected(item);
    }


    public  void listaMapa(){
        int listUsuario = modelo.listParqueaderos.size();

        LatLng latlocal = new LatLng(modelo.latitud, modelo.longitud);// colombia

        if(listUsuario > 0) {

            PolylineOptions pop;

            // Initialize the SDK

            for (int i = 0; i < listUsuario; i++) {
                LatLng latclient = new LatLng(modelo.listParqueaderos.get(i).getLat(), modelo.listParqueaderos.get(i).getLon());
                // double dista = CalculationByDistance(latclient,latlocal );
                Location location = new Location("localizacion 1");
                location.setLatitude(modelo.latitud);  //latitud
                location.setLongitude(modelo.latitud); //longitud
                Location location2 = new Location("localizacion 2");
                location2.setLatitude(modelo.listParqueaderos.get(i).getLat());  //latitud
                location2.setLongitude(modelo.listParqueaderos.get(i).getLon()); //longitud
                double distance = location.distanceTo(location2);


               /* pop= new PolylineOptions();
                pop.width(5).color(Color.BLUE).geodesic(true);;
                pop.add(latlocal);
                pop.add(latclient);
                modelo.mMap.addPolyline(pop);*/



                //modelo.listParqueaderos.get(i).setDistancia(distance);
                createMarker(modelo.listParqueaderos.get(i).getKey(),modelo.listParqueaderos.get(i).getLat(), modelo.listParqueaderos.get(i).getLon(), modelo.listParqueaderos.get(i).getNombreParqueadero(),modelo.mMap);
            }
        }
    }


}