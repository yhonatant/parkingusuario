package com.parkinglot.parkinglot.model.Clases;

import java.util.ArrayList;

public class Usuario {

    private  String key;
    private  String nombre;
    private  String apellido;
    private  String celular;
    private  String correo;
    private  String pasString;
    private  String token;
    private  double latitud;
    private  double longitud;
    private double distancia;
    private double tiempo;
    private  boolean estado;
    private  String placa;
    private  String modelo;
    private  String tarjetaPropiedad;

    //ArrayList<Vehiculo> vehiculos;

    public Usuario(){}

    public Usuario(String key, String nombre, String apellido, String celular, String correo, String pasString, String token, double latitud, double longitud, double distancia, double tiempo, boolean estado, String placa, String modelo, String tarjetaPropiedad) {
        this.key = key;
        this.nombre = nombre;
        this.apellido = apellido;
        this.celular = celular;
        this.correo = correo;
        this.pasString = pasString;
        this.token = token;
        this.latitud = latitud;
        this.longitud = longitud;
        this.distancia = distancia;
        this.tiempo = tiempo;
        this.estado = estado;
        this.placa = placa;
        this.modelo = modelo;
        this.tarjetaPropiedad = tarjetaPropiedad;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPasString() {
        return pasString;
    }

    public void setPasString(String pasString) {
        this.pasString = pasString;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTarjetaPropiedad() {
        return tarjetaPropiedad;
    }

    public void setTarjetaPropiedad(String tarjetaPropiedad) {
        this.tarjetaPropiedad = tarjetaPropiedad;
    }
}
