package com.parkinglot.parkinglot.model.Clases;

public class Parqueadero {

    private  String  key;
    private  String  nombre;
    private  String  nombreParqueadero;
    private  String  celular;
    private  String  direccion;
    private  boolean estado;
    private  double lat;
    private  double lon;
    private  String  horario;
    private  double  precio;
    private  int  cupos;
    private  int  Disponibles;
    private  String  token;


    public Parqueadero(){}


    public Parqueadero(String key, String nombre, String nombreParqueadero, String celular, String direccion, boolean estado, double lat, double lon, String horario, double precio, int cupos, int disponibles, String token) {
        this.key = key;
        this.nombre = nombre;
        this.nombreParqueadero = nombreParqueadero;
        this.celular = celular;
        this.direccion = direccion;
        this.estado = estado;
        this.lat = lat;
        this.lon = lon;
        this.horario = horario;
        this.precio = precio;
        this.cupos = cupos;
        Disponibles = disponibles;
        this.token = token;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreParqueadero() {
        return nombreParqueadero;
    }

    public void setNombreParqueadero(String nombreParqueadero) {
        this.nombreParqueadero = nombreParqueadero;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCupos() {
        return cupos;
    }

    public void setCupos(int cupos) {
        this.cupos = cupos;
    }

    public int getDisponibles() {
        return Disponibles;
    }

    public void setDisponibles(int disponibles) {
        Disponibles = disponibles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
