package com.parkinglot.parkinglot.model.Comandos;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.parkinglot.parkinglot.model.Clases.Parqueadero;
import com.parkinglot.parkinglot.model.Modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ComandoParqueadero {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();


    //interface del listener de la actividad interesada
    private OnParqueaderosChangeListener mListener;

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnParqueaderosChangeListener {


        void getParqueadero();

    }

    public ComandoParqueadero(OnParqueaderosChangeListener mListener){

        this.mListener = mListener;

    }



    public void  getListParqueadero(){
        //preguntasFrecuentes
        modelo.listParqueaderos.clear();

        DatabaseReference ref = database.getReference("Parqueadero");//ruta path

        ChildEventListener listener = new ChildEventListener(){
            @Override
            public void onChildAdded(DataSnapshot snpar, String s) {
                boolean estado = (boolean) snpar.child("estado").getValue();
                if(estado){
                    Parqueadero par = new Parqueadero();
                    //Long timestamp =  (Long) snpar.child("timestamp").getValue();
                    par.setKey(snpar.getKey());

                    double lattitud = (double)snpar.child("lat").getValue();
                    double longitud = (double)snpar.child("long").getValue();

                    double precio = Double.parseDouble(snpar.child("precio").getValue().toString()) ;
                    int cupos =  Integer.parseInt( snpar.child("cupos").getValue().toString());
                    int disponibles = Integer.parseInt( snpar.child("disponible").getValue().toString());


                    par.setCelular(snpar.child("celular").getValue().toString());
                    par.setCupos(cupos);
                    par.setDireccion(snpar.child("direccion").getValue().toString());
                    par.setDisponibles(disponibles);
                    par.setEstado(estado);
                    par.setHorario(snpar.child("horario").getValue().toString());
                    par.setLat(lattitud);
                    par.setNombre(snpar.child("nombre").getValue().toString());
                    par.setNombreParqueadero(snpar.child("nombreParqueadero").getValue().toString());
                    par.setLon(longitud);
                    par.setPrecio(precio);

                    modelo.listParqueaderos.add(par);
                    mListener.getParqueadero();
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                Log.v("snap","");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.v("remove","");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.v("moved","");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.v("cancel","");
            }
        };



        ref.addChildEventListener(listener);


    }





    /**
     * Para evitar nullpointerExeptions
     */
    private static OnParqueaderosChangeListener sDummyCallbacks = new OnParqueaderosChangeListener()
    {

        @Override
        public void getParqueadero()
        {}

    };
}
