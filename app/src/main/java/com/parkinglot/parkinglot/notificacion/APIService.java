package com.parkinglot.parkinglot.notificacion;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAOY9oTWs:APA91bHT9tmdlaok4uaEj9Hy7bZjrgi8BvR2byOW6kpA5GkUVWlsJxSHZfqIwjg0Q10RplhcI5FJKq-YYhDgi4WOaPJo6Hw978wb-DOMj_lBpfTt_yHRv9DXZOzCpTwd6sza00-Sh1z-" //Clave del servidor
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotifcation(@Body NotificationSender body);
}



