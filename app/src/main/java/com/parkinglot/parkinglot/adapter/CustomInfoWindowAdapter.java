package com.parkinglot.parkinglot.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.parkinglot.parkinglot.R;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private static final String TAG = "CustomInfoWindowAdapter";
    private LayoutInflater inflater;
    String name;
    Context context;

    public CustomInfoWindowAdapter(LayoutInflater inflater, String name, Context context){
        this.inflater = inflater;
        this.name = name;
        this.context = context;
    }

    @Override
    public View getInfoContents(final Marker m) {
        //Carga layout personalizado.
        View v = inflater.inflate(R.layout.infowindow_layout, null);
        String[] info = m.getTitle().split("&");
        String url = m.getSnippet();
        LinearLayout  layutdetalle = (LinearLayout)v.findViewById(R.id.layutdetalle);
        ((TextView)v.findViewById(R.id.info_window_nombre)).setText(this.name);
        ((TextView)v.findViewById(R.id.cupos)).setText("Cupos carro: 2 \nCupos Moto: 1");
        ((TextView)v.findViewById(R.id.horario)).setText("Horario: 8:00 AM - 07:00 PM");

        layutdetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"Hola",Toast.LENGTH_LONG).show();
            }
        });
        return v;
    }

    @Override
    public View getInfoWindow(Marker m) {
        return null;
    }

}
